package co.com.isoft.curso.dockerkubernetes.provider.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/v1/service1")
public class BuildingController {

    @GetMapping("/provider")
    public String welcome() {
        return "Bienvenido a mi mundo, soy proveedor ...";
    }
}
