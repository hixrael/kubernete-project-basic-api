package co.com.isoft.curso.dockerkubernetes.consumer.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/v1/service2")
public class BuildingController {

    @GetMapping("/consumer")
    public String welcome() {
        return "Bienvenido a mi mundo, soy consumidor ...";
    }
}
