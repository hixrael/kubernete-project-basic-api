Existe tres microservices:
1. gateway-api: Este microservices es la puerta de entrada a los dos microservices, la url son esta
provider: http://localhost/rest/v1/service1/provider
consumer: http://localhost/rest/v1/service2/consumer
2. provider-api: Este microservices solo mostrara un mensaje indicando que es un provider
3. consumer-api: Este microservices solo mostrara un mensaje indicando que es un consumer

NOTA: Antes de ejecutar el docker-compose-local.yml a cada microservices (gateway-api, provider-api, consumer-api) se debe hacer un clear y luego
un bootjar, con eso se construira los jar para luego hacer el proceso de orquestar (Kubernete) en la local
